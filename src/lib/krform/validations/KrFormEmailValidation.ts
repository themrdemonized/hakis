import { KrFormValidation } from "../KrForm";

export default class KrFormEmailValidation implements KrFormValidation {

    errorText: string = `Пожалуйста, введите E-mail верного формата`;

    isValid(value: string | boolean): boolean
    {
        if(value === undefined || value === null || value === '' || typeof value === 'boolean')
        {
            return true;
        }

        const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return value.match(pattern) !== null;
    }

}
