import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import TradeArticlesManager from "../lib/trade/TradeArticlesManager";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: (to, from, next) => {
      TradeArticlesManager.getArticles({ category_id: 1 }); // force loading data to cache
      next();
    }
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: () => import(/* webpackChunkName: "contacts" */ '../views/Contacts.vue')
  },
  {
    path: '/services',
    name: 'Services',
    component: () => import(/* webpackChunkName: "services" */ '../views/Services.vue'),
    beforeEnter: (to, from, next) => {
      TradeArticlesManager.getArticles({ limit: 20, category_id: 3 }); // force loading data to cache
      next();
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    beforeEnter: (to, from, next) => {
      TradeArticlesManager.getArticles({ limit: 1, category_id: 1 }); // force loading data to cache
      next();
    }
  },
  { path: "/:pathMatch(.*)*", component: () => import(/* webpackChunkName: "page_not_found" */ '../views/PageNotFound.vue') }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
